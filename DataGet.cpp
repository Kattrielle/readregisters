/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DataGet.cpp
 * Author: naldaevaen
 * 
 * Created on 2 Февраль 2016 г., 11:30
 */

#include "DataGet.h"

DataGet::DataGet( std::string devAddr, int addr )
{
    slaveAddr = addr;
    deviceAddress = devAddr;
}

DataGet::DataGet( const DataGet& orig )
{
}

DataGet::~DataGet( )
{
    if ( port != NULL )
    {
        modbus_free( port );
    }
}

int DataGet::Connect( )
{
    port = modbus_new_rtu( deviceAddress.c_str(), BASE_BAUDRATE, BASE_PARITY,
            BASE_DATABITS, BASE_STOPBITS );
    if ( port == NULL )
    {
        return RESULT_ERROR;
    }
    modbus_set_slave( port, slaveAddr );
    if ( modbus_connect( port ) == RESULT_ERROR )
    {
        printf( "Error while connecting to device\n\r" );
        modbus_free( port );
        return RESULT_ERROR;
    }
    return 0;
}

int DataGet::GetLongValues( uint16_t **result )
{
    *result = new uint16_t[REGISTER_LONG_COUNT];
    int resLength = modbus_read_input_registers( port, REGISTER_LONG,
            REGISTER_LONG_COUNT, *result );
    if ( resLength == RESULT_ERROR )
    {
        printf( "Error:%s\n\r", modbus_strerror( errno ));
        delete[] (*result);
        *result = NULL;
        return RESULT_ERROR;
    }
    return resLength;
}
