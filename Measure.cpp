/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Measure.cpp
 * Author: kate
 * 
 * Created on 2 февраля 2016 г., 23:15
 */

#include "Measure.h"

Measure::Measure( )
{
}

//Measure::Measure( uint16_t* slow, uint8_t* fast, int fastLen, int slowLen,
//        int ultra, int magnet, int vibration )
//{
//}

Measure::Measure( bool logsNeed )
{
    time( &timeMeasure );
    event = 0;
    registers = NULL;
    slowLength = 0;
    logs = logsNeed;
}

//Measure::Measure( const Measure& orig )
//{
//}

Measure::~Measure( )
{
    delete[] registers;
}

void Measure::SetMeasureValues( uint16_t* slow, int slowLen, int *baseEvent )
{
    SelectNeededRegisters( slow, slowLen );
    if ( slowLen > REGISTER_EVENTS )
    {
        event = slow[ REGISTER_EVENTS ];
    }
    isEventHappened = ( event - *baseEvent ) && ( *baseEvent >= 0 );
    *baseEvent = event;
    delete[] slow;
}

void Measure::SelectNeededRegisters( uint16_t* baseValues, int baseLen )
{
    int neededRegisters[] = { 0, 1, 2, 5, 6, 7, 8, 10 };
    slowLength = sizeof ( neededRegisters) / sizeof ( int);
    registers = new uint16_t[ slowLength ];
    for ( int i = 0; i < slowLength; i++ )
    {
        registers[i] = (baseLen > neededRegisters[i]) ?
                baseValues[ neededRegisters[i] ] : 0;
    }
    ShowRegisters( neededRegisters, registers );
}

time_t Measure::GetTime( )
{
    return timeMeasure;
}

uint16_t* Measure::GetLongValues( )
{
    return registers;
}

int Measure::GetLongValuesLen( )
{
    return slowLength;
}

int Measure::IsEventHappened()
{
    return isEventHappened;
}

void Measure::ShowRegisters( int* numbers, uint16_t* values )
{
    if ( !logs )
    {
        return;
    }
    for ( int i = 0; i < slowLength; i++ )
    {
        std::cout << numbers[i] + 1 << ": " << (uint16_t) values[i] << ", ";
    }
    std::cout << "end" << std::endl;
}
