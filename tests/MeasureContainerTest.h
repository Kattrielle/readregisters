/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   MeasureContainerTest.h
 * Author: naldaevaen
 *
 * Created on 10.02.2016, 13:37:36
 */

#ifndef MEASURECONTAINERTEST_H
#define MEASURECONTAINERTEST_H

#include <cppunit/extensions/HelperMacros.h>
#include "../MeasuresContainer.h"

class MeasureContainerTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(MeasureContainerTest);

    CPPUNIT_TEST(testHasPointsToSaveNoEvents);
    CPPUNIT_TEST(testHasPointsToSaveHasEvent);
    CPPUNIT_TEST(testTryToGetSavedData);
//    CPPUNIT_TEST(testGetPointsToSave);

    CPPUNIT_TEST_SUITE_END();

public:
    MeasureContainerTest();
    virtual ~MeasureContainerTest();
    void setUp();
    void tearDown();

private:
    void testHasPointsToSaveNoEvents();
    void testHasPointsToSaveHasEvent();
    void testTryToGetSavedData();
//    void testGetPointsToSave();

};

#endif /* MEASURECONTAINERTEST_H */

