/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   MeasureContainerTest.cpp
 * Author: naldaevaen
 *
 * Created on 10.02.2016, 13:37:36
 */

#include "MeasureContainerTest.h"

CPPUNIT_TEST_SUITE_REGISTRATION( MeasureContainerTest );

MeasureContainerTest::MeasureContainerTest( )
{
}

MeasureContainerTest::~MeasureContainerTest( )
{
}

void MeasureContainerTest::setUp( )
{
}

void MeasureContainerTest::tearDown( )
{
}

void MeasureContainerTest::testHasPointsToSaveNoEvents( )
{
    MeasuresContainer measuresContainer( 5 );
    for ( int i = 0; i < 10; i++ )
    {
        uint16_t *slow = new uint16_t[ 25 ];
        for ( int i = 0; i < 25; i++ )
        {
            slow[i] = i;
        }
        measuresContainer.AddMeasure( slow, 25, false );
    }
    CPPUNIT_ASSERT_EQUAL( 0, measuresContainer.HasPointsToSave( ) );
}

void MeasureContainerTest::testHasPointsToSaveHasEvent( )
{
    MeasuresContainer measuresContainer( 5 );
    for ( int i = 0; i < 10; i++ )
    {
        uint16_t *slow = new uint16_t[ 25 ];
        for ( int i = 0; i < 25; i++ )
        {
            slow[i] = i;
        }
        measuresContainer.AddMeasure( slow, 25, false );
    }
    
    uint16_t *slow = new uint16_t[ 25 ];
    for ( int i = 0; i < 25; i++ )
    {
        slow[i] = i;
    }
    slow[2] = 1001;
    measuresContainer.AddMeasure( slow, 25, false );
    
    for ( int i = 0; i < 10; i++ )
    {
        uint16_t *slow = new uint16_t[ 25 ];
        for ( int i = 0; i < 25; i++ )
        {
            slow[i] = i;
        }
        measuresContainer.AddMeasure( slow, 25, false );
    }
    CPPUNIT_ASSERT_EQUAL( 1, measuresContainer.HasPointsToSave( ) );
}

void MeasureContainerTest::testTryToGetSavedData( )
{
    MeasuresContainer measuresContainer( 5 );
    for ( int i = 0; i < 10; i++ )
    {
        uint16_t *slow = new uint16_t[ 25 ];
        for ( int i = 0; i < 25; i++ )
        {
            slow[i] = i;
        }
        measuresContainer.AddMeasure( slow, 25, false );
    }

    uint16_t *slow = new uint16_t[ 25 ];
    for ( int i = 0; i < 25; i++ )
    {
        slow[i] = i;
    }
    slow[2] = 1001;
    measuresContainer.AddMeasure( slow, 25, false );

    for ( int i = 0; i < 6; i++ )
    {
        uint16_t *slow = new uint16_t[ 25 ];
        for ( int i = 0; i < 25; i++ )
        {
            slow[i] = i;
        }
        measuresContainer.AddMeasure( slow, 25, false );
    }

    measuresContainer.GetPointsToSave( );
    slow = new uint16_t[ 25 ];
    for ( int i = 0; i < 25; i++ )
    {
        slow[i] = i;
    }
    measuresContainer.AddMeasure( slow, 25, false );

    CPPUNIT_ASSERT_EQUAL( 0, measuresContainer.HasPointsToSave( ) );
}

//void MeasureContainerTest::testGetPointsToSave( )
//{
//    MeasuresContainer measuresContainer;
//    std::list<Measure*> result = measuresContainer.GetPointsToSave( );
//    if ( true /*check result*/ )
//    {
//        CPPUNIT_ASSERT( false );
//    }
//}

