/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   MeasureTest.h
 * Author: kate
 *
 * Created on 08.02.2016, 20:35:27
 */

#ifndef MEASURETEST_H
#define MEASURETEST_H

#include <cppunit/extensions/HelperMacros.h>
#include "../Measure.h"

class MeasureTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(MeasureTest);

    CPPUNIT_TEST(testFastValueLen);
    CPPUNIT_TEST(testSlowValueLen);
    CPPUNIT_TEST(testEvent);
    CPPUNIT_TEST(testEventHappened);
    CPPUNIT_TEST(testFastBigArrayLen);

    CPPUNIT_TEST_SUITE_END();

public:
    MeasureTest();
    virtual ~MeasureTest();
    void setUp();
    void tearDown();

private:
    void testFastValueLen();
    void testSlowValueLen();
    void testEvent();
    void testEventHappened();
    void testFastBigArrayLen();
};

#endif /* MEASURETEST_H */

