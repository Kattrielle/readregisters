/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   MeasureTest.cpp
 * Author: kate
 *
 * Created on 08.02.2016, 20:35:28
 */

#include "MeasureTest.h"

CPPUNIT_TEST_SUITE_REGISTRATION(MeasureTest);

MeasureTest::MeasureTest() {
}

MeasureTest::~MeasureTest() {
}

void MeasureTest::setUp() {
    
}

void MeasureTest::tearDown() {
}

void MeasureTest::testFastValueLen() { 
    uint16_t *slow = new uint16_t[ 25 ];
    for ( int i = 0; i < 25; i++ )
    {
        slow[i] = i;
    }
    uint8_t *fast = (uint8_t *) malloc( 10 );
    std::fill_n( fast, 10, 5 );
    Measure *measure = new Measure( 1000, 100, 100 );
    measure->SetMeasureValues( slow, fast, 10, 25 );

    CPPUNIT_ASSERT_EQUAL( 10, measure->GetFastValuesLen() );
}

void MeasureTest::testSlowValueLen() {
    uint16_t *slow = new uint16_t[ 25 ];
    for ( int i = 0; i < 25; i++ )
    {
        slow[i] = i;
    }
    uint8_t *fast = (uint8_t *) malloc( 10 );
    std::fill_n( fast, 10, 5 );
    Measure *measure = new Measure( 1000, 100, 100 );
    measure->SetMeasureValues( slow, fast, 10, 25 );

    CPPUNIT_ASSERT_EQUAL( 14, measure->GetLongValuesLen() );
}

void MeasureTest::testEvent() {
    uint16_t *slow = new uint16_t[ 25 ];
    for ( int i = 0; i < 25; i++ )
    {
        slow[i] = i;
    }
    uint8_t *fast = (uint8_t *) malloc( 10 );
    std::fill_n( fast, 10, 5 );
    Measure *measure = new Measure( 1000, 100, 100 );
    measure->SetMeasureValues( slow, fast, 10, 25 );
    
    CPPUNIT_ASSERT_EQUAL( 0, measure->IsEventHappened() );
}

void MeasureTest::testEventHappened( )
{
//    uint16_t slow[] = { 0, 1, 1001, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };
    uint16_t *slow = new uint16_t[ 25 ];
    for ( int i = 0; i < 25; i++ )
    {
        slow[i] = i;
    }
    slow[2] = 1001; // превышение уставки
    
    uint8_t *fast = (uint8_t *) malloc( 10 );
    std::fill_n( fast, 10, 5 );
    Measure *measure = new Measure( 1000, 100, 100 );
    measure->SetMeasureValues( slow, fast, 10, 25 );
    
    CPPUNIT_ASSERT_EQUAL( 1, measure->IsEventHappened() );
}

void MeasureTest::testFastBigArrayLen( )
{
    uint16_t *slow = new uint16_t[ 25 ];
    for ( int i = 0; i < 25; i++ )
    {
        slow[i] = i;
    }
    uint8_t *fast = (uint8_t *) malloc( 752 );
    std::fill_n( fast, 752, 5 );
    Measure *measure = new Measure( 1000, 100, 100 );
    measure->SetMeasureValues( slow, fast, 752, 25 );
    
    CPPUNIT_ASSERT_EQUAL( 752, measure->GetFastValuesLen() );
}

