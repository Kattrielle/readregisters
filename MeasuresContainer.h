/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MeasuresContainer.h
 * Author: naldaevaen
 *
 * Created on 3 Февраль 2016 г., 15:27
 */

#ifndef MEASURESCONTAINER_H
#define MEASURESCONTAINER_H

#include <list>
#include "stdio.h"
#include "Measure.h"

//Набор получаемых значений
class MeasuresContainer {
public:
    MeasuresContainer();
    MeasuresContainer( int round );
    MeasuresContainer(const MeasuresContainer& orig);
    virtual ~MeasuresContainer();
    
    Measure *AddMeasure(uint16_t *slow, int slowLength, bool logsNeed ); //Добавление считанного с датчика замера
    std::list<Measure> GetPointsToSave( bool allPoints ); //Вызов функции для записи значений
    int HasPointsToSave(); //Указывает, появился ли набор значений около события
private:
    std::list<Measure> measurePoints; //Текущий список считанных замеров
    std::list<Measure> pointsToSave; //Список, отправляемый в файл
    int numRoundPoints; //Число сохраняемых точек вокруг события
    int pointsBefore; //Находящееся в списке число точек до события
    int pointsAfter; //Находящееся в списке число точек после события
    int eventCounter; //Текущее значение регистра событий 
    bool containsEvent; //В списке имеется замер с событием
    
    void PushEventMeasure(); //Добавлен замер с событием
    void PushNonEventMeasure( ); //Добавлен обычный замер
    void SaveMeasures(); //Отправление списка текущих замеров в список для файла
};

#endif /* MEASURESCONTAINER_H */

