/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Measure.h
 * Author: kate
 *
 * Created on 2 февраля 2016 г., 23:15
 */

#ifndef MEASURE_H
#define MEASURE_H

#include <modbus/modbus.h>
#include "time.h"
#include "stdio.h"
#include <string>
#include <ctime>
#include <iostream>
#include <stdlib.h>

#define REGISTER_EVENTS 10

#define TO_HOURS 60
#define TO_DAYS 1440

// Данные по одному замеру
class Measure {
public:
    Measure();
    Measure( bool logsNeed );
//    Measure(const Measure& orig);
    virtual ~Measure();
    
    void SetMeasureValues( uint16_t *slow, int slowLen, int *baseEvent ); //Заполнение замера данными
    int IsEventHappened(); //Проверка, есть ли сработавшее событие в выборке
    time_t GetTime();
    uint16_t *GetLongValues( );
    int GetLongValuesLen();
private:
    uint16_t *registers; //Значения "полезных" input-регистров
    uint8_t *fastValues; //Набор быстрых замеров
    int event; //Сигнализатор события
    bool isEventHappened; //Изменилось ли значение eventCounter
    time_t timeMeasure; //Время снятия замера
    int slowLength; //Длина массива "медленных" регистров
    int fastLength; //Длина массива быстрых замеров
    
    bool logs; //Указание, требуется ли выводить на экран логи
    
    void SelectNeededRegisters( uint16_t *baseValues, int baseLen );
    void ShowRegisters( int *numbers, uint16_t *values );
};

#endif /* MEASURE_H */

