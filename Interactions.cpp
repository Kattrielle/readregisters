/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Interactions.cpp
 * Author: naldaevaen
 * 
 * Created on 4 Февраль 2016 г., 11:51
 */

#include "Interactions.h"

Interactions::Interactions( )
{
}

Interactions::Interactions( const Interactions& orig )
{
}

Interactions::Interactions( std::string path, std::string pathFullLogs, 
        std::string devAddr, int addr, int numPoints, int interval, 
        bool allPts, bool logsNeed )
{
    dataGet = new DataGet( devAddr, addr );
    measures = new MeasuresContainer( numPoints );
    fileWork = new WriteToFile( path, pathFullLogs );
    msInterval = interval;
    allPoints = allPts;
    needShowLogs = logsNeed;
    if ( dataGet->Connect( ) )
    {
        exit( EXIT_FAILURE );
    }
}

Interactions::~Interactions( )
{
    std::list<Measure> lst = measures->GetPointsToSave( allPoints );
    fileWork->WriteMeasures( lst );
    dataGet->~DataGet( );
    measures->~MeasuresContainer( );
    fileWork->~WriteToFile( );
}

timespec Interactions::CountDelay( long timeBefore, long timeAfter )
{
    double workedSeconds = ((double) (timeAfter - timeBefore)) / CLOCKS_PER_SEC;
    double delayNeed = ((double) msInterval) / 1000.0 - workedSeconds;
    int secondsNeed = (int) trunc( delayNeed );
    long nanosecondsNeed = (long) (modf( delayNeed, &delayNeed ) * 1000000000);
    
    struct timespec delayValue = {secondsNeed, nanosecondsNeed};
    return delayValue;
}

void Interactions::SaveData( )
{
    std::list<Measure> lst = measures->GetPointsToSave( allPoints );
    fileWork->WriteMeasures( lst );
    lst.clear( );
}

int Interactions::CollectData( )
{
    uint16_t *slow;
    int lengthSlow;
    Measure *meas;
    lengthSlow = dataGet->GetLongValues( &slow );
    meas = measures->AddMeasure( slow, lengthSlow, needShowLogs );
    fileWork->WriteOneFullMeasure( meas );
    if ( measures->HasPointsToSave( ) && !allPoints )
    {
        SaveData( );
    }
}

void Interactions::DoObserves( )
{
    while ( true )
    {
        long timeBefore = clock( );
        CollectData( );
        long timeAfter = clock( );
        
        struct timespec delayValue = CountDelay( timeBefore, timeAfter );
        nanosleep( &delayValue, NULL );
    }
}

void Interactions::DoObserves( int numOfPoints )
{
    std::string command = "pkill readregisters";
    system( command.c_str() );
    for ( int i = 0; i < numOfPoints; i++ )
    {
        long timeBefore = clock( );
        CollectData( );
        long timeAfter = clock( );
        
        struct timespec delayValue = CountDelay( timeBefore, timeAfter );
        nanosleep( &delayValue, NULL );
    }
    SaveData();
}
