/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MeasuresContainer.cpp
 * Author: naldaevaen
 * 
 * Created on 3 Февраль 2016 г., 15:27
 */

#include "MeasuresContainer.h"

MeasuresContainer::MeasuresContainer( )
{
}

MeasuresContainer::MeasuresContainer(int round)
{
    numRoundPoints = round;
    pointsBefore = 0;
    pointsAfter = 0;
    eventCounter = -1;
    containsEvent = false;
}

MeasuresContainer::MeasuresContainer( const MeasuresContainer& orig )
{
}

MeasuresContainer::~MeasuresContainer( )
{
}

Measure *MeasuresContainer::AddMeasure(uint16_t* slow, int slowLength, bool logsNeed)
{
    Measure el( logsNeed );
    measurePoints.push_back( el );
    measurePoints.back().SetMeasureValues( slow, slowLength, &eventCounter );
    if ( measurePoints.back().IsEventHappened( ) )
    {
        PushEventMeasure();
    } else 
    {
        PushNonEventMeasure();
    }
    return &measurePoints.back();
}

void MeasuresContainer::PushEventMeasure( )
{
    pointsAfter = 0;
    containsEvent = true;
}

void MeasuresContainer::PushNonEventMeasure( )
{
    if ( containsEvent )
    {
        pointsAfter++;
        if (pointsAfter >= numRoundPoints )
        {
            SaveMeasures();
        }
    } else if ( pointsBefore < numRoundPoints )
    {
        pointsBefore++;
    } else 
    {
        measurePoints.pop_front();
    }
}

void MeasuresContainer::SaveMeasures( )
{
    pointsToSave.splice( pointsToSave.end(), measurePoints );
    pointsBefore = 0;
    pointsAfter = 0;
    containsEvent = false;
}

int MeasuresContainer::HasPointsToSave( )
{
    return !pointsToSave.empty();
}

//at this moment - with clearing list
std::list<Measure> MeasuresContainer::GetPointsToSave( bool allPoints )
{
    std::list<Measure> result = *(new std::list<Measure>);
    if ( allPoints )
    {
        result.splice( result.begin(), measurePoints );
    } else {
        result.splice( result.begin(), pointsToSave );
    }
//    result.splice( result.begin(), measurePoints );
    return result;
}

