/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Interactions.h
 * Author: naldaevaen
 *
 * Created on 4 Февраль 2016 г., 11:51
 */

#ifndef INTERACTIONS_H
#define INTERACTIONS_H

#include "time.h"
#include "math.h"
#include "MeasuresContainer.h"
#include "DataGet.h"
#include "WriteToFile.h"

class Interactions {
public:
    Interactions();
    Interactions(std::string path, std::string pathFullLogs, std::string devAddr,
            int addr, int numPoints, int interval, bool allPts, bool logsNeed);
    Interactions(const Interactions& orig);
    virtual ~Interactions();

    void DoObserves(); //Цикл получения данных, мс
    void DoObserves(int numOfPoints); //Получение данных по определенному числу замеров
private:
    DataGet *dataGet; //Связь с устройством
    MeasuresContainer *measures; //Собираемые с устройства замеры
    WriteToFile *fileWork; //Запись в .csv

    int msInterval; //Время между замерами
    bool allPoints; //Сохранять все точки или только события
    bool needShowLogs; //Выдавать ли значения в консоль

    void SaveData(); //Сохранение замеров из буфера на сохранение
    int CollectData(); //Считывание 1 замера с утсройства
    timespec CountDelay(long timeBefore, long timeAfter); //Расчет времени ожидания системы
};

#endif /* INTERACTIONS_H */

