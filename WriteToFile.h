/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WriteToFile.h
 * Author: naldaevaen
 *
 * Created on 5 Февраль 2016 г., 10:24
 */

#ifndef WRITETOFILE_H
#define WRITETOFILE_H

#define FORMAT_FILENAME "%d.%m.%Y-%H.%M.%S"
#define FORMAT_CSVSTRING "%d.%m.%Y %H:%M:%S"
#define FORMAT_DAY "%d.%m.%Y"
#define FILE_EXTENSION ".csv"
#define DIVIDER ";"
#define ENDLINE "\n";

#define TEXT_ADAPTER "Adapter;\"1;magnitometr;COM7;115200;0;1;8;1000;1000;1000\";;;;;;;;;;;;;"
#define TEXT_ADDRESSES "Adresses;0;1;2;3;4;5;6;7"
#define TEXT_ADAPTERS "Adapters;0;0;0;0;0;0;0;0"
#define TEXT_DEVADDRESSES "DevAdresses;1;1;1;1;1;1;1;1"
#define TEXT_FORMATS "Formats;0;0;0;0;0;0;0;0"
#define TEXT_TYPES "Types;2;2;2;2;2;2;2;2"
#define TEXT_NAMES "Names;State;TDC_Middle;TDC_Delta;Timer;Error;TDC_Stop_1;TDC_Stop_0;Event_Counter"
#define TEXT_REGISTERS ";\"300001h\x0D\x0Amagnitometr - 1\";\"300002h\x0D\x0Amagnitometr - 1\";\"300003h\x0D\x0Amagnitometr - 1\";\"300006h\x0D\x0Amagnitometr - 1\";\"300007h\x0D\x0Amagnitometr - 1\";\"300008h\x0D\x0Amagnitometr - 1\";\"300009h\x0D\x0Amagnitometr - 1\";\"300011h\x0D\x0Amagnitometr - 1\""

#include <fstream>
#include <list>
#include <unistd.h>
#include "time.h"
#include "Measure.h"

class WriteToFile {
public:
    WriteToFile();
    WriteToFile(std::string path, std::string fullLogsDir );
    WriteToFile(const WriteToFile& orig);
    virtual ~WriteToFile();
    void WriteMeasures(std::list<Measure> &measures); //Отправка в файл сохраненных значений
    void WriteOneFullMeasure(Measure *measure); //Отправка в общий файл замеров текущего значения
private:
    std::string filePath; //Путь к файлам логов
    std::string filePathFullLogs; //Путь к файлам общего лога
    std::ofstream fileSlowData; //Файл с набором значений "медленных" регистров. Совместим с TikModscan
    std::ofstream fileAllMeasures; //Файл для записи общего лога. Совместим с TikModscan

    void WriteOneMeasure(std::ofstream *file, Measure &value, std::string timeFormat); //Отправка в файл 1 сохраненнго значения
    void OpenFile(std::ofstream *file, std::string dir, std::string time); //Открытие файлов для записи в них набора значений около события
    void CloseFiles(); //Закрытие файлов
    std::string FormatTime(time_t time, std::string format); //Представление времени в виде строки
    std::string FindEventTime(std::list<Measure> &points); //Время события для названия файла
};

#endif /* WRITETOFILE_H */

