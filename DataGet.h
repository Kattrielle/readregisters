/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DataGet.h
 * Author: naldaevaen
 *
 * Created on 2 Февраль 2016 г., 11:30
 */

#ifndef DATAGET_H
#define DATAGET_H

#include <modbus/modbus.h>
#include "errno.h"
#include "stdio.h"
#include <string>
#include <cstring>
#include "stdlib.h"
#include <iostream>

#define BASE_BAUDRATE 115200
#define BASE_PARITY 'N'
#define BASE_DATABITS 8
#define BASE_STOPBITS 1

#define RESULT_ERROR -1

//Адреса регистров начинаются с 0!!! Не как логические регистры!
#define REGISTER_LONG 0
#define REGISTER_LONG_COUNT 11

class DataGet {
public:
    DataGet(std::string devAddr, int addr);
    DataGet(const DataGet& orig);
    virtual ~DataGet();

    int Connect( ); //Подключение к устройству
    int GetLongValues( uint16_t **result ); //Получение набора регистров отдельных значений (input)
private:
    modbus_t *port; //Основной класс для работы с COM-портом
    int slaveAddr; //Адрес опрашиваемого устройства
    std::string deviceAddress; //Адрес COM-порта

};

#endif /* DATAGET_H */

