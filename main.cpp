/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: naldaevaen
 *
 * Created on 2 Февраль 2016 г., 9:56
 */

#include <cstdlib>
#include "stdio.h"
#include "libModbus/modbus.h"
#include "DataGet.h"
#include "Measure.h"
#include "MeasuresContainer.h"
#include "Interactions.h"
#include <getopt.h>
#include <signal.h>

#define BASE_SLAVE_ADDR 1
#define BASE_NUMPOINTS 20
#define BASE_INTERVAL 100
#define BASE_DEV_ADDRESS "/dev/ttyUSB0"
#define BASE_LOGS_PATH "/var/log/services/"
#define BASE_ADDITIONAL_LOGS "full/"
#define BASE_WRITE_NUMPOINTS 10

using namespace std;

Interactions *wrk; //можно ли что-то с ним сделать, чтобы он тут не маячил?

void terminateHandler( int signal )
{
    printf( "Terminating process\n" );
    if ( wrk )
    {
        wrk->~Interactions( );
    }
    exit( EXIT_SUCCESS );
}

void InitializeInputsBaseValues( int *addr, int* points, int* interval,
        string *deviceAddr, string *logs, string *fullLogs )
{
    *addr = BASE_SLAVE_ADDR;
    *points = BASE_NUMPOINTS;
    *interval = BASE_INTERVAL;
    *deviceAddr = BASE_DEV_ADDRESS;
    *logs = BASE_LOGS_PATH;
    *fullLogs = BASE_ADDITIONAL_LOGS;
}

void ShowHelp( )
{
    printf( "The list of options:\n" );
    printf( " -l, --logs  : Allows to show logs of values on the screen\n" );
    printf( " -L, --logs-path  : The place to save logs\n" );
    printf( "May be empty (full logs will be saved at logs directory)\n" );
    printf( " -d, --device  : The address of device port\n" );
    printf( " -s, --slave  : The address of slave device\n" );
    printf( " -i, --interval  : The interval between measures, ms\n" );
    printf( " -M, --measures  : Number of measures near the event\n" );
    printf( " -w, --write-measures  : Number of measures to write to file\n" );
    printf( " -h, --help  : Display this help and exit\n" );
    printf( "\nDefault values:" );
    printf( " logs-path=%s\n", BASE_LOGS_PATH );
    printf( " full-logs=%s\n", BASE_ADDITIONAL_LOGS );
    printf( " device=%s\n", BASE_DEV_ADDRESS );
    printf( " slave=%d\n", BASE_SLAVE_ADDR );
    printf( " interval=%d ms\n", BASE_INTERVAL );
    printf( " measures=%d\n", BASE_NUMPOINTS );
    printf( " write-measures=%d\n", BASE_WRITE_NUMPOINTS );
}

/*
 * 
 */
int main( int argc, char** argv )
{
    //Задание обработки внешних сигналов
    struct sigaction sigAct;
    sigAct.sa_handler = terminateHandler;
    sigaction( SIGTERM, &sigAct, 0 );
    sigaction( SIGINT, &sigAct, 0 );

    sigset_t newset;
    sigemptyset( &newset );
    sigaddset( &newset, SIGHUP );
    sigprocmask( SIG_BLOCK, &newset, 0 );

    //Инициализация default значениями
    bool logsRequired = false;
    bool writeAllMeasures = false;
    int slaveAddr;
    int points;
    int interval;
    int writeNum;
    string logsPath;
    string fullLogsPath;
    string deviceAddress;
    InitializeInputsBaseValues( &slaveAddr, &points, &interval, &deviceAddress,
            &logsPath, &fullLogsPath );

    //    //Разбор входных параметров
    const char* shortOptions = "lL:f::d:s:M:i:w::h";
    const struct option longOptions[] = {
        {"logs", no_argument, NULL, 'l'},
        {"logs-path", required_argument, NULL, 'L'},
        {"full-logs", optional_argument, NULL, 'f'},
        {"device", required_argument, NULL, 'd'},
        {"slave", required_argument, NULL, 's'},
        {"measures", required_argument, NULL, 'M'},
        {"interval", required_argument, NULL, 'i'},
        {"write-measures", optional_argument, NULL, 'w'},
        {"help", no_argument, NULL, 'h'},
        {NULL, 0, NULL, 0}
    };
    int result;
    int optionIndex;
    while ( (result = getopt_long( argc, argv, shortOptions, longOptions, &optionIndex )) != -1 )
    {
        switch ( result )
        {
            case 'l':
                logsRequired = true;
                break;
            case 'L':
                logsPath = optarg;
                break;
            case 'f':
                if ( optarg != NULL )
                {
                    fullLogsPath = optarg;
                }
                break;
            case 'd':
                deviceAddress = optarg;
                break;
            case 's':
                slaveAddr = atoi( optarg );
                break;
            case 'M':
                points = atoi( optarg );
                break;
            case 'i':
                interval = atoi( optarg );
                break;
            case 'w':
                writeAllMeasures = true;
                if ( optarg != NULL )
                {
                    writeNum = atoi( optarg );
                }
                break;
            case 'h':
                ShowHelp( );
                exit( EXIT_SUCCESS );
            case '?':
            default:
                printf( "Found unknown option\n" );
                printf( "Try -h or --help\n" );
                exit( EXIT_FAILURE );
        }
    }

    cout << "Starting..." << endl;
    wrk = new Interactions( logsPath, fullLogsPath, deviceAddress, slaveAddr, 
            points, interval, writeAllMeasures, logsRequired );
    if ( writeAllMeasures )
    {
        wrk->DoObserves( writeNum );
    }
    wrk->DoObserves( );
    wrk->~Interactions( );
    return 0;
}
