/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WriteToFile.cpp
 * Author: naldaevaen
 * 
 * Created on 5 Февраль 2016 г., 10:24
 */

#include "WriteToFile.h"

WriteToFile::WriteToFile( )
{
}

WriteToFile::WriteToFile( std::string path, std::string fullLogsDir )
{
    filePath = path;
    filePathFullLogs = filePath + fullLogsDir;
    std::string command = "mkdir -p " + path + " " + filePathFullLogs;
    system( command.c_str( ) );
    command = "chmod 777 " + path + " " + filePathFullLogs;
    system( command.c_str( ) );
    if ( access( filePath.c_str(), F_OK ) || 
            access( filePathFullLogs.c_str(), F_OK ))
    {
        printf( "I've lost the output directory\n" );
        exit( EXIT_FAILURE );
    }
}

WriteToFile::WriteToFile( const WriteToFile& orig )
{
}

WriteToFile::~WriteToFile( )
{
    CloseFiles( );
}

std::string WriteToFile::FormatTime( time_t timeValue, std::string format )
{
    struct tm *time = localtime( &timeValue );
    char result[20];
    strftime( result, 100, format.c_str( ), time );
    return result;
}

std::string WriteToFile::FindEventTime( std::list<Measure> &points )
{
    time_t neededTime = points.front( ).GetTime( );
    for ( std::list<Measure>::iterator el = points.begin( );
            el != points.end( ); ++el )
    {
        if ( (*el).IsEventHappened( ) )
        {
            neededTime = (*el).GetTime( );
            break;
        }
    }
    return FormatTime( neededTime, FORMAT_FILENAME );
}

void WriteToFile::OpenFile( std::ofstream *file, std::string dir, std::string time )
{
    std::string filename = dir + time + FILE_EXTENSION;
    if ( access( filename.c_str(), F_OK ) )
    {
        std::cout << "Creating a file " << time + FILE_EXTENSION << std::endl;
        file->open( filename.c_str( ) );
        *file << TEXT_ADAPTER << ENDLINE;
        *file << TEXT_ADDRESSES << ENDLINE;
        *file << TEXT_ADAPTERS << ENDLINE;
        *file << TEXT_DEVADDRESSES << ENDLINE;
        *file << TEXT_FORMATS << ENDLINE;
        *file << TEXT_TYPES << ENDLINE;
        *file << TEXT_NAMES << ENDLINE;
        *file << TEXT_REGISTERS << ENDLINE;
    } else
    {
        file->open( filename.c_str( ), std::ofstream::out | std::ofstream::app );
    }
}

void WriteToFile::CloseFiles( )
{
    fileSlowData.close( );
    fileAllMeasures.close( );
}

void WriteToFile::WriteMeasures( std::list<Measure> &measures )
{
    if ( measures.empty( ) )
    {
        printf( "There were no points to save\n\r" );
        return;
    }
    std::string fileName = FindEventTime( measures );
    OpenFile( &fileSlowData, filePath, fileName );
    for ( std::list<Measure>::iterator meas = measures.begin( );
            meas != measures.end( ); ++meas )
    {
        WriteOneMeasure( &fileSlowData, *meas, FORMAT_CSVSTRING );
    }
    CloseFiles( );
}

void WriteToFile::WriteOneFullMeasure( Measure* measure )
{
    std::string time = FormatTime( measure->GetTime(), FORMAT_DAY );
    OpenFile( &fileAllMeasures, filePathFullLogs, time );
    WriteOneMeasure( &fileAllMeasures, *measure, FORMAT_CSVSTRING );
    CloseFiles();
}

void WriteToFile::WriteOneMeasure( std::ofstream *file, Measure &value, 
        std::string timeFormat )
{
    int length;
    std::string time = FormatTime( value.GetTime( ), timeFormat );
    *file << time << DIVIDER;
    uint16_t *slowValues;
    slowValues = value.GetLongValues( );
    length = value.GetLongValuesLen( );
    for ( int i = 0; i < length - 1; i++ )
    {
        *file << slowValues[i] << DIVIDER;
    }
    *file << ((length > 0) ? slowValues[ length - 1 ] : 0) << ENDLINE;
}
